package model;

public class Card {
	private int balance;

	public Card() {
		balance = 100;
	}
	
	public int add(int amount){
		balance = balance + amount;
		return amount;
	}
	
	public int buy(int amount){
		balance = balance - amount;
		return amount;
	}
	
	public int checkBalance(){
		return balance;
	}
	
	public String toString() {
		return "Balance = "+ Integer.toString(balance);
	}

}
